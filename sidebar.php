				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php 
								wp_nav_menu(array(
								   	'container' => false,
								   	'menu' => __( 'News', 'bonestheme' ),
								   	'menu_class' => 'news-nav',
								   	'theme_location' => 'news-nav',
								   	'before' => '',
								   	'after' => '',
								   	'depth' => 2,
								   	'items_wrap' => '<h3>News</h3> <ul>%3$s</ul>'
								));
							?>
						</nav>
					</div>
				</div>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Seminars in Teaching Excellence subpage
								if (is_tree(1555) || get_field('menu_select') == "seminars") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Seminars', 'bonestheme' ),
										'menu_class' => 'seminars-nav',
										'theme_location' => 'seminars-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Seminars in Teaching Excellence</h3> <ul>%3$s</ul>'
									));
								}
								// If an Inclusive Classrooms subpage
								if (is_tree(3143) || get_field('menu_select') == "inclusive") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Inclusive Classrooms', 'bonestheme' ),
										'menu_class' => 'inclusive-nav',
										'theme_location' => 'inclusive-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Past Initiatives</h3> <ul>%3$s</ul>'
									));
								}
								// If a Resources subpage
								if (is_tree(4341) || get_field('menu_select') == "resources") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Resources', 'bonestheme' ),
										'menu_class' => 'resources-nav',
										'theme_location' => 'resources-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Resources</h3> <ul>%3$s</ul>'
									));
								}
								// If a About subpage
								if (is_tree(858) || get_field('menu_select') == "about") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About Us', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>About Us</h3> <ul>%3$s</ul>'
									));
								}
								// If an Current Initiatives subpage
								if ( //is_tree(6139) || 
									// To let Parosl not inherit this menu
									get_field('menu_select') == "current-initiatives") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Current Initiatives', 'bonestheme' ),
									   	'menu_class' => 'current-initiatives-nav',
									   	'theme_location' => 'current-initiatives-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Current Initiatives</h3> <ul>%3$s</ul>'
									));
								}
								// If an Cross-Campus Collaborations subpage
								if (is_tree(5739) || get_field('menu_select') == "collaboration") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Cross-Campus Collaborations', 'bonestheme' ),
									   	'menu_class' => 'collaboration-nav',
									   	'theme_location' => 'collaboration-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Cross-Campus Collaborations</h3> <ul>%3$s</ul>'
									));
								}
								// If a Parosl subpage
								if (is_tree(6798) || get_field('menu_select') == "parosl") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Parosl', 'bonestheme' ),
									   	'menu_class' => 'parosl-nav',
									   	'theme_location' => 'parosl-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Parosl</h3> <ul>%3$s</ul>'
									));
								}
								// If an News subpage
								if (is_tree(5672) || is_tree(3821) || get_field('menu_select') == "news") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'News', 'bonestheme' ),
									   	'menu_class' => 'news-nav',
									   	'theme_location' => 'news-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>News</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>News</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>